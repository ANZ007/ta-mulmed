from flask import Flask, request, render_template
from werkzeug.utils import secure_filename
import flask.scaffold
flask.helpers._endpoint_from_view_func = flask.scaffold._endpoint_from_view_func
from flask_restful import Api, Resource, abort
import os, uuid, time, cv2
# import tensorflow as tf
# import tensorflow.keras.preprocessing.image as keras_pre_img
from tensorflow.keras.models import load_model
import numpy as np
import PIL

app = Flask(__name__)
app.config['JSON_SORT_KEYS'] = False
app.config['UPLOAD_EXTENSIONS'] = ['.jpg', '.jpeg', '.png']
api = Api(app)

IMAGE_SIZE = (150, 150)
labels = ['Normal', 'Pneumonia']
MODEL_PATH = 'model/model.h5'
model = load_model(MODEL_PATH, compile=False)

def cleanup_files():
    path = "static"
    prefix_file = "img-"
    now = time.time()

    for f in filter(lambda fname: 'img-' in fname, os.listdir(path)):
        f = os.path.join(path, f)
        if os.stat(f).st_mtime < now - 5 * 60:
            if os.path.isfile(f):
                os.remove(f)
                print("File {} removed".format(f))

def image_preprocessor(path):
    img_RGB = cv2.imread(path)
    b, g, r = cv2.split(img_RGB)
    img_RGB = cv2.merge([r, g, b])
    img = cv2.resize(img_RGB, IMAGE_SIZE)
    img = img/255.0
    img = np.reshape(img, (1, 150, 150, 3))
    return img

def model_predict(image):
    # print("Image_shape", image.shape)
    # print("Image_dimension", image.ndim)
    # Returns Probability:

    predict_prob = float(model.predict(image)[0])
    # Returns class:
    predict_class = model.predict_classes(image)[0]
    '''    if prediction == 1:
        return "Pneumonia"
    else:
        return "Normal"'''
    return (predict_class,predict_prob)

class Predict_Json(Resource):
    def post(self):
        cleanup_files()
        uniq_fn = str(uuid.uuid4())
        img = request.files['img']
        fn_img = secure_filename(img.filename)
        if fn_img != '':
            fn_img_ext = os.path.splitext(fn_img)[1]
            if fn_img_ext not in app.config['UPLOAD_EXTENSIONS']:
                abort(400, message="File Type not Allowed.")
            img.save("static/img-{}.jpg".format(uniq_fn))
            # Preprocessing Image
            image = image_preprocessor("static/img-{}.jpg".format(uniq_fn))
            # Performing Prediction
            predict = model_predict(image)

            return {"labels": str(labels[int(predict[0])]),"accuracy": str(predict[1])}

        else:
            abort(400, message="File Not Found.")

api.add_resource(Predict_Json, "/json-predict")

@app.route('/', methods=['GET', 'POST'])
def index():
    if flask.request.method == 'GET':
        cleanup_files() 
        return render_template("index.html", image="static/not_submitted.jpg", predict="display:none;")

    elif flask.request.method == 'POST':
        uniq_fn = str(uuid.uuid4())
        img = request.files['img']
        fn_img = secure_filename(img.filename)
        if fn_img != '':
            fn_img_ext = os.path.splitext(fn_img)[1]
            if fn_img_ext not in app.config['UPLOAD_EXTENSIONS']:
                return render_template("index.html", image="static/not_allowed.jpg", prediction="File type not Allowed...")
            img.save("static/img-{}.jpg".format(uniq_fn))
            # Preprocessing Image
            image = image_preprocessor("static/img-{}.jpg".format(uniq_fn))
            # Performing Prediction
            predict = model_predict(image)

            # return {"labels": str(predict[0]),"accuracy": str(predict[1])}

            return render_template("index.html", image="img-{}.jpg".format(uniq_fn), predict_result=str(labels[int(predict[0])]), predict_per=str(predict[1]*100))
        else:
            return render_template("index.html", image="static/not_found.jpg", predict="display:none;")

if __name__ == "__main__":
    app.run(host= '0.0.0.0', port=5000,debug=False)
